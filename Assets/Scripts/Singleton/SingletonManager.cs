﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mw.singleton
{
    public class SingletonManager
    {

        public static void InitSingletons()
        {
            _ = PlayerManager.Instance;
        }

    }
}