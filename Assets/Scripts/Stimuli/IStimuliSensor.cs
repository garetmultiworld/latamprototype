﻿using UnityEngine;

public interface IStimuliSensor
{
    void Stimuli(string stimuli, GameObject source);
}
