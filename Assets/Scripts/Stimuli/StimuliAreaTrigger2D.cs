﻿using UnityEngine;

public class StimuliAreaTrigger2D : TriggerInterface
{
#if UNITY_EDITOR
    public bool showStimuliAreaTrigger = true;
#endif

    public string Stimuli;
    public float Radius;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), Radius);
        StimuliSensor Sensor;
        foreach (Collider2D hitCollider in hitColliders)
        {
            Sensor = hitCollider.gameObject.GetComponent<StimuliSensor>();
            if (Sensor != null)
            {
                Sensor.Stimuli(Stimuli, gameObject);
            }
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (" + Stimuli + ", " + Radius.ToString() + ")";
        return desc;
    }
}
