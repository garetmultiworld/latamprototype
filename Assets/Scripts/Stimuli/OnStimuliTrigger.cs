﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OnStimuliTrigger
{

    public string Stimuli="";
    public TriggerInterface Trigger;

}
