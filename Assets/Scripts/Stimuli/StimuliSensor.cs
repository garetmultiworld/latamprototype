﻿using UnityEngine;

public class StimuliSensor : MonoBehaviour, IStimuliSensor
{

    public OnStimuliTrigger[] StimuliTriggers=new OnStimuliTrigger[0];

    public void Stimuli(string stimuli,GameObject source)
    {
        if(!enabled || !gameObject.activeSelf)
        {
            return;
        }
        foreach (OnStimuliTrigger trigger in StimuliTriggers)
        {
            if (trigger.Stimuli.Equals(stimuli))
            {
                trigger.Trigger.Fire();
            }
        }
    }

}
