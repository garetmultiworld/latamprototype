﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class SpriteRendererColorTrigger: TriggerInterface
{
    public SpriteRenderer spriteRenderer;
    public bool Animate;
    public Color NewColor;
    [Range(0f, 5f)]
    public float Speed;
    public TriggerInterface TriggerOnComplete;
    public AnimationCurve AnimCurve;

    private bool IsAnimating = false;
    private Color StartingColor;
    private bool NewIsGreaterR;
    private bool NewIsGreaterG;
    private bool NewIsGreaterB;
    private bool NewIsGreaterA;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (!Animate)
        {
            spriteRenderer.color= new Color(NewColor.r, NewColor.g, NewColor.b, NewColor.a);
        }
        else
        {
            NewIsGreaterR = NewColor.r > spriteRenderer.color.r;
            NewIsGreaterG = NewColor.g > spriteRenderer.color.g;
            NewIsGreaterB = NewColor.b > spriteRenderer.color.b;
            NewIsGreaterA = NewColor.a > spriteRenderer.color.a;
            IsAnimating = true;
        }
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        float timeLapse;
        float r, g, b, a;
        int complete=0;
        if (NewIsGreaterR)
        {
            if (NewColor.r > spriteRenderer.color.r)
            {
                if (NewColor.r <= 0.01 && NewColor.r >= -0.01)
                {
                    timeLapse = spriteRenderer.color.r / 0.01f;
                }
                else
                {
                    timeLapse = spriteRenderer.color.r / NewColor.r;
                }
                r = spriteRenderer.color.r + Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                r = NewColor.r;
                complete++;
            }
        }
        else
        {
            if (NewColor.r < spriteRenderer.color.r)
            {
                if (spriteRenderer.color.r <= 0.01 && spriteRenderer.color.r >= -0.01)
                {
                    timeLapse = NewColor.r / 0.01f;
                }
                else
                {
                    timeLapse = NewColor.r / spriteRenderer.color.r;
                }
                r = spriteRenderer.color.r - Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                r = NewColor.r;
                complete++;
            }
        }
        if (NewIsGreaterG)
        {
            if (NewColor.g > spriteRenderer.color.g)
            {
                if (NewColor.g <= 0.01 && NewColor.g >= -0.01)
                {
                    timeLapse = spriteRenderer.color.g / 0.01f;
                }
                else
                {
                    timeLapse = spriteRenderer.color.g / NewColor.g;
                }
                g = spriteRenderer.color.g + Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                g = NewColor.g;
                complete++;
            }
        }
        else
        {
            if (NewColor.g < spriteRenderer.color.g)
            {
                if (spriteRenderer.color.g <= 0.01 && spriteRenderer.color.g >= -0.01)
                {
                    timeLapse = NewColor.g / 0.01f;
                }
                else
                {
                    timeLapse = NewColor.g / spriteRenderer.color.g;
                }
                g = spriteRenderer.color.g - Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                g = NewColor.g;
                complete++;
            }
        }
        if (NewIsGreaterB)
        {
            if (NewColor.b > spriteRenderer.color.b)
            {
                if (NewColor.b <= 0.01 && NewColor.b >= -0.01)
                {
                    timeLapse = spriteRenderer.color.b / 0.01f;
                }
                else
                {
                    timeLapse = spriteRenderer.color.b / NewColor.b;
                }
                b = spriteRenderer.color.b + Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                b = NewColor.b;
                complete++;
            }
        }
        else
        {
            if (NewColor.b < spriteRenderer.color.b)
            {
                if (spriteRenderer.color.b <= 0.01 && spriteRenderer.color.b >= -0.01)
                {
                    timeLapse = NewColor.b / 0.01f;
                }
                else
                {
                    timeLapse = NewColor.b / spriteRenderer.color.b;
                }
                b = spriteRenderer.color.b - Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                b = NewColor.b;
                complete++;
            }
        }
        if (NewIsGreaterA)
        {
            if (NewColor.a > spriteRenderer.color.a)
            {
                if (NewColor.a <= 0.01 && NewColor.a >= -0.01)
                {
                    timeLapse = spriteRenderer.color.a / 0.01f;
                }
                else
                {
                    timeLapse = spriteRenderer.color.a / NewColor.a;
                }
                a = spriteRenderer.color.a + Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                a = NewColor.a;
                complete++;
            }
        }
        else
        {
            if (NewColor.a < spriteRenderer.color.a)
            {
                if (spriteRenderer.color.a <= 0.01 && spriteRenderer.color.a >= -0.01)
                {
                    timeLapse = NewColor.a / 0.01f;
                }
                else
                {
                    timeLapse = NewColor.a / spriteRenderer.color.a;
                }
                a = spriteRenderer.color.a - Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                a = NewColor.a;
                complete++;
            }
        }
        spriteRenderer.color = new Color(r, g, b, a);
        if (complete==4)
        {
            IsAnimating = false;
            if (TriggerOnComplete != null)
            {
                TriggerOnComplete.Fire(this);
            }
        }
    }

}
