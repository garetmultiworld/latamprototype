﻿using UnityEngine;

public class RectBounds: MonoBehaviour
{

    public enum AnchorType
    {
        None,
        Top,
        Left,
        Bottom,
        Right
    }

    public Rect TheRect;
    public Color outline;
    public Color fill;
    public string Label;

    public Vector2 GetTopLeft()
    {
        return new Vector2(TheRect.x - (TheRect.width / 2), TheRect.y - (TheRect.height / 2));
    }

    public Vector2 GetBottomRight()
    {
        return new Vector2(TheRect.x + (TheRect.width / 2), TheRect.y + (TheRect.height / 2));
    }

    public float GetBottom()
    {
        return TheRect.y - (TheRect.height / 2);
    }

    public float GetRight()
    {
        return TheRect.x + (TheRect.width / 2);
    }

    public float GetTop()
    {
        return TheRect.y + (TheRect.height / 2);
    }

    public float GetLeft()
    {
        return TheRect.x - (TheRect.width / 2);
    }
}
