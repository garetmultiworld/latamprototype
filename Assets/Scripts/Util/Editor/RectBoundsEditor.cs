﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RectBounds))]
public class RectBoundsEditor : Editor
{
    void OnSceneGUI()
    {
        var rectExample = (RectBounds)target;

        var rect = RectUtils.ResizeRect(
            rectExample.TheRect,
            Handles.CubeHandleCap,
            rectExample.outline,
            rectExample.fill,
            HandleUtility.GetHandleSize(Vector3.zero) * .1f,
            .1f);

        rectExample.TheRect = rect;
    }
}
