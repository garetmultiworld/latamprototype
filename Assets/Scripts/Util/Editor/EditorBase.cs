﻿using UnityEditor;

public class EditorBase : Editor
{

    protected bool _hadChanges = false;

    public bool HadChanges { get => _hadChanges; set => _hadChanges = value; }

}
