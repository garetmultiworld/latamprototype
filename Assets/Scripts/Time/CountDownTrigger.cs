﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using System;

public class CountDownTrigger : TriggerInterface
{

    public float TimeMultiplier = 1;
    public float StartTime=0;
    public float TotalTime;
    public TriggerInterface OnFinish;
    public Text MinuteText;
    public Text SecondText;
    public Text MilisecondText;
    public bool DisplayInSeconds;
    public Slider slider;

    protected bool IsPlaying = false;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        if (slider != null)
        {
            slider.maxValue = TotalTime;
        }
        SetLabel(TotalTime - StartTime);
        StartCoroutine(CountDown());
    }

    private void SetLabelSeconds(float time)
    {
        if (SecondText != null)
        {
            int seconds = (int)Math.Floor(time);
            if (seconds < 10)
            {
                SecondText.text = "0" + seconds.ToString();
            }
            else
            {
                SecondText.text = seconds.ToString();
            }
        }
    }

    private void SetLabel(float time)
    {
        if (slider != null)
        {
            slider.value = time;
        }
        if (DisplayInSeconds)
        {
            SetLabelSeconds(time);
            return;
        }
        int minutes = (int)Math.Floor(time / 60);
        if (MinuteText != null)
        {
            if (minutes < 10)
            {
                MinuteText.text = "0" + minutes.ToString();
            }
            else
            {
                MinuteText.text = minutes.ToString();
            }
        }
        if (SecondText != null)
        {
            int seconds = (int)Math.Floor(time - (minutes * 60));
            if (seconds < 10)
            {
                SecondText.text = "0" + seconds.ToString();
            }
            else
            {
                SecondText.text = seconds.ToString();
            }
        }
        if (MilisecondText != null)
        {
            int miliseconds = ((int)Math.Floor(time * 100)) - (((int)Math.Floor(time)) * 100);
            if (miliseconds < 10)
            {
                MilisecondText.text = "0" + miliseconds.ToString();
            }
            else
            {
                MilisecondText.text = miliseconds.ToString();
            }
        }
    }

    private IEnumerator CountDown()
    {
        float time;
        for (time = TotalTime - StartTime; time > 0 && IsPlaying; time -= (Time.deltaTime*TimeMultiplier)) 
        {
            SetLabel(time);
            yield return null;
        }
        if (IsPlaying)
        {
            if (time < 0)
            {
                time = 0;
            }
            SetLabel(time);
            if (OnFinish != null)
            {
                OnFinish.Fire(this);
            }
        }
    }

}
