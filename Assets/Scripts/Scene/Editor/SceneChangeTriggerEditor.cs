﻿using UnityEditor;

[CustomEditor(typeof(SceneChangeTrigger), true)]
public class SceneChangeTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(SceneChangeTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "SceneChangeTrigger");
    }

    protected void StorePrefabConflict(SceneChangeTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        SceneChangeTrigger sceneChangeTrigger = (SceneChangeTrigger)target;
        SetUpPrefabConflict(sceneChangeTrigger);
        sceneChangeTrigger.showSceneChangeTrigger = EditorGUILayout.Foldout(
            sceneChangeTrigger.showSceneChangeTrigger,
            "Scene"
        );
        if (sceneChangeTrigger.showSceneChangeTrigger)
        {
            string prevString = sceneChangeTrigger.SceneToLoad;
            sceneChangeTrigger.SceneToLoad = EditorGUILayout.TextField("Scene To Load", sceneChangeTrigger.SceneToLoad);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(sceneChangeTrigger.SceneToLoad)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(sceneChangeTrigger.SceneToLoad)) ||
                (prevString != null && !prevString.Equals(sceneChangeTrigger.SceneToLoad))
            )
            {
                _hadChanges = true;
            }
            prevString = sceneChangeTrigger.NextStartingPoint;
            sceneChangeTrigger.NextStartingPoint = EditorGUILayout.TextField("Starting Point", sceneChangeTrigger.NextStartingPoint);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(sceneChangeTrigger.NextStartingPoint)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(sceneChangeTrigger.NextStartingPoint)) ||
                (prevString != null && !prevString.Equals(sceneChangeTrigger.NextStartingPoint))
            )
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(sceneChangeTrigger);
    }
}
