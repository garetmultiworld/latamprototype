﻿using UnityEngine.SceneManagement;

public class SceneChangeTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showSceneChangeTrigger = true;
#endif

    public string SceneToLoad;
    public string NextStartingPoint;

    public override void Cancel()
    {}

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        InterSceneManager.Instance.NextStartingPoint = NextStartingPoint;
        InterSceneManager.Instance.NextScene = SceneToLoad;
        SceneManager.LoadScene(SceneToLoad);
    }
}
