﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InterSceneManager : MonoBehaviour
{

    #region Static Instance
    private static InterSceneManager instance;
    public static InterSceneManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<InterSceneManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned InterSceneManager", typeof(InterSceneManager)).GetComponent<InterSceneManager>();
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    [HideInInspector]
    public string NextStartingPoint;
    [HideInInspector]
    public string NextScene;

    private GameObject LevelStart;

    void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name.Equals(NextScene))
        {
            var startingPoints = FindObjectsOfType<StartingPoint>();
            foreach (StartingPoint startingPoint in startingPoints)
            {
                if (startingPoint.Label.Equals(NextStartingPoint))
                {
                    PlayerManager.Instance.GetPlayer(0).transform.position = startingPoint.transform.position;
                    if (startingPoint.TriggerOnArrive != null)
                    {
                        startingPoint.TriggerOnArrive.Fire();
                    }
                    break;
                }
            }
        }
    }

}
