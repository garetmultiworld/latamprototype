﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ChangeLightColorTrigger : TriggerInterface
{

    public Light2D light2D;
    public Color color;
    public bool Animate;
    public float MinTime = 1;
    public float MaxTime = 2;
    public AnimationCurve AnimCurve;
    public TriggerInterface TriggerOnComplete;

    private bool IsAnimating = false;
    private Color StartingColor;
    private float TotalTime;
    private float CurrentTimer = 0;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (Animate)
        {
            StartingColor = new Color(light2D.color.r, light2D.color.g, light2D.color.b, light2D.color.a);
            TotalTime = UnityEngine.Random.Range(MinTime, MaxTime);
            CurrentTimer = 0;
            IsAnimating = true;
        }
        else
        {
            light2D.color = new Color(color.r, color.g, color.b, color.a);
        }
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        float t = CurrentTimer / TotalTime;
        Color newColor= new Color(
            StartingColor.r + ((StartingColor.r - StartingColor.r) * AnimCurve.Evaluate(t)),
            StartingColor.g + ((StartingColor.g - StartingColor.g) * AnimCurve.Evaluate(t)),
            StartingColor.b + ((StartingColor.b - StartingColor.b) * AnimCurve.Evaluate(t)),
            StartingColor.a + ((StartingColor.a - StartingColor.a) * AnimCurve.Evaluate(t))
        );
        CurrentTimer += Time.deltaTime;
        if (CurrentTimer >= TotalTime)
        {
            newColor.r = color.r;
            newColor.g = color.g;
            newColor.b = color.b;
            newColor.a = color.a;
            IsAnimating = false;
            if (TriggerOnComplete != null)
            {
                TriggerOnComplete.Fire(this);
            }
        }
        light2D.color = newColor;
    }

}
