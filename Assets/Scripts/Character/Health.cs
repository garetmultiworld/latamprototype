﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{

    public Slider slider;
    public float MaxHealth=100;
    public float InitialHealth=100;
    public TriggerInterface OnDamageTrigger;
    public TriggerInterface OnDeathTrigger;

    protected float CurrentHealth;

    void Start()
    {
        CurrentHealth = InitialHealth;
        if (slider != null)
        {
            slider.value = CurrentHealth;
        }
    }

    public void SetHealth(float amount)
    {
        CurrentHealth = amount;
        RunValidations();
    }

    public void ReceiveDirectDamage(float amount)
    {
        CurrentHealth -= amount;
        if (OnDamageTrigger != null)
        {
            OnDamageTrigger.Fire();
        }
        RunValidations();
    }

    protected void RunValidations()
    {
        if (CurrentHealth <= 0 && OnDeathTrigger != null)
        {
            OnDeathTrigger.Fire();
        }
        if (slider != null)
        {
            slider.value = CurrentHealth;
        }
    }
}
