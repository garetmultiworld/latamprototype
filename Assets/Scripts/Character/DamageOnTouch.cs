﻿using System.Collections;
using UnityEngine;

public class DamageOnTouch : MonoBehaviour
{

    public TriggerInterface OnHitTrigger;
    public float Amount=1;
    public LayerMask FilterLayer;
    public float ReloadTime=1;

    protected bool _reloading=false;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (_reloading)
        {
            return;
        }
        Health health=c.GetComponent<Health>();
        if (health != null && (((1 << c.gameObject.layer) & FilterLayer) != 0))
        {
            health.ReceiveDirectDamage(Amount);
            _reloading = true;
            StartCoroutine(Reload());
            if (OnHitTrigger != null)
            {
                OnHitTrigger.Fire();
            }
        }
    }

    private IEnumerator Reload()
    {
        for(float currentTime = 0; currentTime < ReloadTime; currentTime+=Time.deltaTime)
        {
            yield return null;
        }
        _reloading = false;
    }

}
