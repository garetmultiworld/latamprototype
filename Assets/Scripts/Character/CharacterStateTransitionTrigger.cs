﻿namespace mw.player
{
    public class CharacterStateTransitionTrigger : TriggerInterface
    {

        public CharacterState ParentState;
        public string Transition;

        public override void Cancel()
        {
        }

        public override void Fire()
        {
            if (!CanTrigger())
            {
                return;
            }
            CharacterStateTransition transition = ParentState.GetTransition(Transition);
            if (transition.BeforeTransition != null)
            {
                transition.BeforeTransition.Fire(this);
            }
            ParentState.GetCharacter().ChangeState(transition.NextState);
        }
    }
}