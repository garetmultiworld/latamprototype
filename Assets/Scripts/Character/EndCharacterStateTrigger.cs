﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mw.player
{
    public class EndCharacterStateTrigger : TriggerInterface
    {

        public Character character;

        public override void Cancel()
        {
        }

        public override void Fire()
        {
            if (!CanTrigger())
            {
                return;
            }
            character.EndState();
        }
    }
}
