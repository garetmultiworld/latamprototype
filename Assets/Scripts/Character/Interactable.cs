﻿using mw.player;
using UnityEngine;

public class Interactable : MonoBehaviour
{

    public TriggerInterface Trigger;

    public void Interact(Character character)
    {
        if (Trigger != null)
        {
            Trigger.EventSource = character;
            Trigger.Fire();
        }
    }
}
