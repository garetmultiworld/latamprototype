﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlayerAction : TriggerInterface
{

    public int PlayerNumber = 1;
    public Player.Actions Action;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        PlayerManager.Instance.ProcessPlayerAction(PlayerNumber - 1, Action);
    }
}
