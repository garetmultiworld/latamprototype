﻿using mw.player;
using UnityEngine;

public class InputMove3DPlaneState : CharacterState
{

    public float Speed = 1f;

    protected Rigidbody _rigidBody=null;
    protected Vector3 VectorInput=new Vector3();
    protected Animator animator;

    public override void OnEnterState()
    {
        if (_rigidBody == null)
        {
            _rigidBody = character.GetComponent<Rigidbody>();
            animator = character.GetComponent<Animator>();
        }
    }

    public override void OnExitState()
    {
    }

    public override void OnFixedUpdate()
    {
        _rigidBody.velocity = VectorInput;
    }

    public override void OnPauseState()
    {
    }

    public override void OnResumeState()
    {
    }

    public override void OnUpdate()
    {
        VectorInput = new Vector3(Input.GetAxis("Horizontal") * Speed, _rigidBody.velocity.y, Input.GetAxis("Vertical") * Speed);
        if (
            VectorInput.x > 0.01 || VectorInput.x < -0.01 ||
            VectorInput.z > 0.01 || VectorInput.z < -0.01
        )
        {
            if (animator != null)
            {
                animator.SetFloat("xInput", VectorInput.x);
                animator.SetFloat("zInput", VectorInput.z);
            }
            character.transform.LookAt(character.transform.position + new Vector3(VectorInput.x, 0, VectorInput.z));
        }
    }
}
