﻿using mw.player;

public class IdleState : CharacterState
{
    public override void OnEnterState()
    {
    }

    public override void OnExitState()
    {
    }

    public override void OnFixedUpdate()
    {
    }

    public override void OnPauseState()
    {
    }

    public override void OnResumeState()
    {
    }

    public override void OnUpdate()
    {
    }
}
