﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTrigger : TriggerInterface
{

    public TriggerInterface TriggerA;
    public TriggerInterface TriggerB;
    public bool FiresB=false;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (FiresB)
        {
            TriggerB.Fire(this);
            FiresB = false;
        }
        else
        {
            TriggerA.Fire(this);
            FiresB = true;
        }
    }
}
