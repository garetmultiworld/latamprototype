﻿using UnityEngine;

public abstract class TriggerInterface : MonoBehaviour
{

#if UNITY_EDITOR
    [HideInInspector]
    public bool showTriggerInterface;
#endif

    [Tooltip("If this trigger is disabled it won't fire")]
    public bool IsDisabled;
    [Tooltip("The name of this trigger, doesn't have to be unique")]
    public new string name;
    [Tooltip("The maximum amount of times this trigger can be activated, 0 or negative for unlimited")]
    public int MaxActivationTimes=0;

    [HideInInspector]
    public GameObject EventSource = null;
    [HideInInspector]
    public string StringParameter="";
    [HideInInspector]
    public float FloatParameter = 0;

    protected int ActivationTimes = 0;

    public bool IgnoreDisabled = false;

    protected bool CanTrigger()
    {
        if (
            IsDisabled ||
            (MaxActivationTimes > 0 && ActivationTimes >= MaxActivationTimes) || 
            (
                !IgnoreDisabled&&
                (!enabled || !gameObject.activeSelf)
            )
        )
        {
            return false;
        }
        ActivationTimes++;
        return true;
    }

    public void Fire(TriggerInterface trigger)
    {
        EventSource = trigger.EventSource;
        StringParameter = trigger.StringParameter;
        FloatParameter = trigger.FloatParameter;
        Fire();
    }

    abstract public void Fire();

    abstract public void Cancel();

    public string GetLabel()
    {
        string lab = name;
        if (IsDisabled)
        {
            lab += ", disabled";
        }
        if (MaxActivationTimes>0)
        {
            lab += ", max "+ MaxActivationTimes.ToString() + " times";
        }
        return lab;
    }

    virtual public string GetDescription()
    {
        return GetLabel();
    }

}
