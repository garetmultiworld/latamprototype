﻿public class GroupTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showGroupTrigger=true;
#endif

    public TriggerInterface[] Triggers= { };

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (TriggerInterface trigger in Triggers)
        {
            if (trigger != null)
            {
                trigger.FloatParameter = FloatParameter;
                trigger.Fire(this);
            }
        }
    }

    public override void Cancel()
    {
        foreach (TriggerInterface trigger in Triggers)
        {
            if (trigger != null)
            {
                trigger.Cancel();
            }
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (" + Triggers.Length.ToString() + " elements, ";
        int assigned = 0;
        foreach(TriggerInterface trigger in Triggers)
        {
            if (trigger != null)
            {
                assigned++;
            }
        }
        desc += assigned.ToString()+" assigned)";
        return desc;
    }

}
