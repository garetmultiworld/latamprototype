﻿using UnityEngine;

public class TriggerIfWithinArea : TriggerInterface
{

    public LayerMask FilterLayer;
    public int Quantity = 1;
    public TriggerInterface TriggerIfWithin;
    public TriggerInterface TriggerIfNotWithin;
    public bool CheckOnEnterAndExit=true;

    protected int _numInCollider=0;

    public void OnTriggerEnter(Collider c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            _numInCollider++;
        }
        Fire();
    }

    public void OnTriggerExit(Collider c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            _numInCollider--;
        }
        Fire();
    }

    public override void Cancel()
    {
        if (_numInCollider>=Quantity)
        {
            if (TriggerIfWithin != null)
            {
                TriggerIfWithin.Cancel();
            }
        }
        else
        {
            if (TriggerIfNotWithin != null)
            {
                TriggerIfNotWithin.Cancel();
            }
        }
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (_numInCollider >= Quantity)
        {
            if (TriggerIfWithin != null)
            {
                TriggerIfWithin.Fire(this);
            }
        }
        else
        {
            if (TriggerIfNotWithin != null)
            {
                TriggerIfNotWithin.Fire(this);
            }
        }
    }

}
