﻿using System;

public class TriggerFireableSequence : TriggerInterface
{

#if UNITY_EDITOR
    public bool showTriggerFireableSequence = true;
#endif

    public TriggerFireableSequenceItem[] Triggers=new TriggerFireableSequenceItem[0] ;
    public bool loop;

    protected int index=-1;

    public override void Cancel()
    {
        FirePrevious();
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        FireNext();
    }

    public void FireNext()
    {
        index++;
        if (index >= Triggers.Length)
        {
            if (loop)
            {
                index = 0;
            }
            else
            {
                return;
            }
        }
        Triggers[index].Fire(this);
    }

    public void FirePrevious()
    {
        index--;
        if (index < 0)
        {
            if (loop)
            {
                index = Triggers.Length-1;
            }
            else
            {
                return;
            }
        }
        Triggers[index].Fire(this);
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (";
        if (loop)
        {
            desc += "loop, ";
        }
        desc+= Triggers.Length.ToString() + " elements, ";
        int assigned = 0;
        foreach (TriggerFireableSequenceItem trigger in Triggers)
        {
            if (trigger.TriggerToFire != null)
            {
                assigned++;
            }
        }
        desc += assigned.ToString() + " assigned)";
        return desc;
    }

}
