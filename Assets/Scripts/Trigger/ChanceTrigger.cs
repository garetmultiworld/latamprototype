﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanceTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showChanceTrigger = true;
#endif

    public TriggerInterface TriggerToFire;
    [Range(0f, 100f)]
    public float Chances = 100f;

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (Random.Range(0f, 100f) <= Chances)
        {
            TriggerToFire.Fire(this);
        }
    }

    public override void Cancel()
    {
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (TriggerToFire == null)
        {
            desc += " (no trigger assigned)";
        }
        else
        {
            desc += " "+Chances.ToString() + "% (" + TriggerToFire.GetLabel() + ")";
        }
        return desc;
    }
}
