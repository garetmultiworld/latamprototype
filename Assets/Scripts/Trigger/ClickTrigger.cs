﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickTrigger : MonoBehaviour
{

    public TriggerInterface Trigger;

    void OnMouseDown()
    {
        Trigger.Fire();
    }
}
