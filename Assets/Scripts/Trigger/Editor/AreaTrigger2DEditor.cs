﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AreaTrigger2D))]
public class AreaTrigger2DEditor : EditorBase
{

    protected void SetUpPrefabConflict(AreaTrigger2D trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "AreaTrigger2D");
    }

    protected void StorePrefabConflict(AreaTrigger2D trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        AreaTrigger2D areaTrigger = (AreaTrigger2D)target;
        SetUpPrefabConflict(areaTrigger);
        bool hasColliders = false;
        Collider2D []colliders = areaTrigger.GetComponents<Collider2D>();
        foreach(Collider2D collider in colliders)
        {
            if (collider.isTrigger)
            {
                hasColliders = true;
            }
        }
        if (!hasColliders)
        {
            EditorGUILayout.HelpBox("This object must have at least one Collider2D with trigger enabled", MessageType.Warning);
            if (GUILayout.Button("Add BoxCollider"))
            {
                BoxCollider2D box = areaTrigger.gameObject.AddComponent(typeof(BoxCollider2D)) as BoxCollider2D;
                box.isTrigger = true;
                _hadChanges = true;
            }
            if (GUILayout.Button("Add CircleCollider"))
            {
                CircleCollider2D circle = areaTrigger.gameObject.AddComponent(typeof(CircleCollider2D)) as CircleCollider2D;
                circle.isTrigger = true;
                _hadChanges = true;
            }
        }
        areaTrigger.FilterLayer=EditorUtils.SelectLayerMask(this,"Detectable Layers", areaTrigger.FilterLayer);
        GUILayout.Label(" ");
        areaTrigger.OnEnter = EditorUtils.SelectTrigger(this,"On Enter", areaTrigger.OnEnter);
        areaTrigger.EnterAndExitExclusive = EditorUtils.Checkbox(this,
            "Enter And Exit Cancel Each Other?", 
            areaTrigger.EnterAndExitExclusive
        );
        areaTrigger.OnExit = EditorUtils.SelectTrigger(this,"On Exit", areaTrigger.OnExit);

        EditorGUILayout.HelpBox("If you want to save the object that enters or exits this area you can save it on its respective target holder.", MessageType.Info);
        areaTrigger.TargetOnEnter = EditorUtils.TargetHolderField(this,
            "On Enter",
            areaTrigger.TargetOnEnter
        );
        areaTrigger.TargetOnExit = EditorUtils.TargetHolderField(this,
            "On Exit",
            areaTrigger.TargetOnExit
        );
        StorePrefabConflict(areaTrigger);
    }

}
