﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CancelTrigger), true)]
public class CancelTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(CancelTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "CancelTrigger");
    }

    protected void StorePrefabConflict(CancelTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        CancelTrigger cancelTrigger = (CancelTrigger)target;
        SetUpPrefabConflict(cancelTrigger);
        cancelTrigger.TriggerToCancel = EditorUtils.SelectTrigger(this,"Trigger To Cancel", cancelTrigger.TriggerToCancel);
        StorePrefabConflict(cancelTrigger);
    }
}
