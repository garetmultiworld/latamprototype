﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ClickTrigger))]
public class ClickTriggerEditor : EditorBase
{

    protected void SetUpPrefabConflict(ClickTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "ClickTrigger");
    }

    protected void StorePrefabConflict(ClickTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("This uses Unity's OnMouseDown method, if the trigger isn't firing there's probably some collider or UI interference.", MessageType.Info);

        ClickTrigger clickTrigger = (ClickTrigger)target;
        SetUpPrefabConflict(clickTrigger);

        clickTrigger.Trigger = EditorUtils.SelectTrigger(this,"On Enter", clickTrigger.Trigger);
        StorePrefabConflict(clickTrigger);
    }
}
