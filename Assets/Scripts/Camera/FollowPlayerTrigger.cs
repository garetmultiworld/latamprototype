﻿using Cinemachine;
using mw.player;

public class FollowPlayerTrigger : TriggerInterface
{
    public CinemachineVirtualCamera cinemachineVirtualCamera;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineVirtualCamera.m_Follow = PlayerManager.Instance.GetPlayer(0).transform;
    }
}
