﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GenericEventListeners
{
    public string EventName;
    public List<GenericEventListener> listeners=new List<GenericEventListener>();

    public void TriggerStringEvent(GameObject source,string eventParameter, float EventNumber)
    {
        foreach(GenericEventListener listener in listeners)
        {
            if (listener != null)
            {
                listener.TriggerStringEvent(source,eventParameter, EventNumber);
            }
        }
    }
}
