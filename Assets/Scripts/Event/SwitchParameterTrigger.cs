﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchParameterTrigger : TriggerInterface
{

    public SwitchParameterCaseTrigger[] Cases;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach(SwitchParameterCaseTrigger theCase in Cases)
        {
            if (theCase != null && theCase.ParameterValue.Equals(StringParameter) && theCase.Trigger != null)
            {
                theCase.Trigger.FloatParameter = FloatParameter;
                theCase.Trigger.Fire(this);
            }
        }
    }
}
