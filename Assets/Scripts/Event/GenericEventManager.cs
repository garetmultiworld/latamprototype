﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericEventManager : MonoBehaviour
{
    #region Static Instance
    private static GenericEventManager instance;
    public static GenericEventManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GenericEventManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned GenericEventManager", typeof(GenericEventManager)).GetComponent<GenericEventManager>();
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    protected Dictionary<string, GenericEventListeners> Listeners=new Dictionary<string, GenericEventListeners>();

    GenericEventManager()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public bool RegisterListener(GenericEventListener listener)
    {
        if (string.IsNullOrEmpty(listener.EventName))
        {
            return false;
        }
        GenericEventListeners genericEventListeners;
        if (!Listeners.ContainsKey(listener.EventName))
        {
            genericEventListeners = new GenericEventListeners();
            Listeners.Add(listener.EventName, genericEventListeners);
        }
        else
        {
            genericEventListeners = Listeners[listener.EventName];
        }
        genericEventListeners.listeners.Add(listener);
        return true;
    }

    public void TriggerStringEvent(GameObject source,string EventName,string EventParameter,float EventNumber)
    {
        if (!Listeners.ContainsKey(EventName))
        {
            return;
        }
        Listeners[EventName].TriggerStringEvent(source,EventParameter, EventNumber);
    }

}
