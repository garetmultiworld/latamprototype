﻿using UnityEngine;
using UnityEngine.UI;

public class UpdateTextFromEventNumberTrigger : TriggerInterface
{

    public Text text;
    public string Prefix;
    public string Suffix;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        text.text = Prefix + FloatParameter.ToString() + Suffix;
    }
}
