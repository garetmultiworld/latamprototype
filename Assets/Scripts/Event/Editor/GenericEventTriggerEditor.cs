﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(GenericEventTrigger))]
public class GenericEventTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(GenericEventTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "GenericEventTrigger");
    }

    protected void StorePrefabConflict(GenericEventTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        GenericEventTrigger genericEventTrigger = (GenericEventTrigger)target;
        SetUpPrefabConflict(genericEventTrigger);
        genericEventTrigger.showGenericEventTrigger = EditorGUILayout.Foldout(
            genericEventTrigger.showGenericEventTrigger,
            "Event"
        );
        if (genericEventTrigger.showGenericEventTrigger)
        {
            string prevString = genericEventTrigger.EventName;
            genericEventTrigger.EventName = EditorGUILayout.TextField("Event To Trigger", genericEventTrigger.EventName);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(genericEventTrigger.EventName)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(genericEventTrigger.EventName)) ||
                (prevString != null && !prevString.Equals(genericEventTrigger.EventName))
            )
            {
                _hadChanges = true;
            }
            prevString = genericEventTrigger.EventParameter;
            genericEventTrigger.EventParameter = EditorGUILayout.TextField("Parameter (aditional info)", genericEventTrigger.EventParameter);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(genericEventTrigger.EventParameter)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(genericEventTrigger.EventParameter)) ||
                (prevString != null && !prevString.Equals(genericEventTrigger.EventParameter))
            )
            {
                _hadChanges = true;
            }
            float prevFloat = genericEventTrigger.EventNumber;
            genericEventTrigger.EventNumber = EditorGUILayout.FloatField("Number (aditional info)", genericEventTrigger.EventNumber);
            if(prevFloat!= genericEventTrigger.EventNumber)
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(genericEventTrigger);
    }

}
