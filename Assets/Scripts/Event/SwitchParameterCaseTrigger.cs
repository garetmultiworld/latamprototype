﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SwitchParameterCaseTrigger 
{

    public string ParameterValue="";
    public TriggerInterface Trigger;
    
}
