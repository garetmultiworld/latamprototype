﻿public class SwitchLanguageTrigger : TriggerInterface
{

    public I18nLanguages Language;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        I18nManager.Instance.CurrentLanguage= Language;
    }
}
