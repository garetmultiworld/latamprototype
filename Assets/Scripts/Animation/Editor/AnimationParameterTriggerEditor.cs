﻿using UnityEditor;

[CustomEditor(typeof(AnimationParameterTrigger))]
public class AnimationParameterTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(AnimationParameterTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "AnimationParameterTrigger");
    }

    protected void StorePrefabConflict(AnimationParameterTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        AnimationParameterTrigger animationParameterTrigger = (AnimationParameterTrigger)target;
        SetUpPrefabConflict(animationParameterTrigger);
        animationParameterTrigger.showAnimationParameterTrigger = EditorGUILayout.Foldout(
            animationParameterTrigger.showAnimationParameterTrigger,
            "Animation Parameter"
        );
        if (animationParameterTrigger.showAnimationParameterTrigger)
        {
            if (animationParameterTrigger.Animator == null)
            {
                EditorGUILayout.HelpBox("There's no animator assigned", MessageType.Warning);
            }
            animationParameterTrigger.Animator = EditorUtils.AnimatorField(
                this,
                "Animator",
                animationParameterTrigger.Animator
            );
            if (string.IsNullOrEmpty(animationParameterTrigger.ParameterName))
            {
                EditorGUILayout.HelpBox("There's no parameter set up", MessageType.Warning);
            }
            animationParameterTrigger.ParameterName = EditorUtils.TextField(this,"Parameter Name", animationParameterTrigger.ParameterName);
            animationParameterTrigger.Type = (AnimationParameterTrigger.ParamTypes)EditorUtils.EnumPopup(this,animationParameterTrigger.Type);
            switch (animationParameterTrigger.Type)
            {
                case AnimationParameterTrigger.ParamTypes.Bool:
                    animationParameterTrigger.BoolValue = EditorUtils.Toggle(this,"Valor", animationParameterTrigger.BoolValue);
                    break;
                case AnimationParameterTrigger.ParamTypes.Float:
                    animationParameterTrigger.FloatValue = EditorUtils.FloatField(this,"Valor", animationParameterTrigger.FloatValue);
                    break;
                case AnimationParameterTrigger.ParamTypes.Integer:
                    animationParameterTrigger.IntegerValue = EditorUtils.IntField(this,"Valor", animationParameterTrigger.IntegerValue);
                    break;
            }
        }
        StorePrefabConflict(animationParameterTrigger);
    }
}
