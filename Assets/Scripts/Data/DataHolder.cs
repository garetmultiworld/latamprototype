﻿using UnityEngine;

public class DataHolder : MonoBehaviour
{

#if UNITY_EDITOR
    [HideInInspector]
    public bool showDataHolder=true;
#endif
    public enum Type
    {
        Text,
        Number,
        Integer,
        Bool
    }

    public string Name;
    public Type type;

    public TriggerInterface OnChangeTrigger;

    [SerializeField]
    protected string InternalValue;
    [SerializeField]
    protected float InternalFloatValue;
    [SerializeField]
    protected int InternalIntValue;
    [SerializeField]
    protected bool InternalBoolValue;

    public virtual string Value
    {
        get { 
            return InternalValue; 
        }
    }

    public virtual float FloatValue
    {
        get
        {
            return InternalFloatValue;
        }
    }

    public virtual int IntValue
    {
        get
        {
            return InternalIntValue;
        }
    }

    public virtual bool BoolValue
    {
        get
        {
            return InternalBoolValue;
        }
    }

    public void SetValue(int newValue)
    {
        InternalIntValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Fire();
        }
    }

    public void SetValue(float newValue)
    {
        InternalFloatValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Fire();
        }
    }

    public void SetValue(string newValue)
    {
        InternalValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Fire();
        }
    }

    public void SetValue(bool newValue)
    {
        InternalBoolValue = newValue;
        if (OnChangeTrigger != null)
        {
            OnChangeTrigger.Fire();
        }
    }

}
