﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DataHolder))]
public class DataHolderEditor : EditorBase
{

    protected void SetUpPrefabConflict(DataHolder trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "DataHolder");
    }

    protected void StorePrefabConflict(DataHolder trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        DataHolder dataHolder = (DataHolder)target;

        SetUpPrefabConflict(dataHolder);

        dataHolder.showDataHolder = EditorGUILayout.Foldout(
            dataHolder.showDataHolder,
            "Data Holder (" + dataHolder.Name+")"
        );
        if (dataHolder.showDataHolder)
        {

            dataHolder.Name = EditorUtils.TextField(this, "Name", dataHolder.Name);

            dataHolder.type = (DataHolder.Type)EditorUtils.EnumPopup(this, dataHolder.type);

            dataHolder.OnChangeTrigger = EditorUtils.SelectTrigger(this, "On Change", dataHolder.OnChangeTrigger);

            switch (dataHolder.type)
            {
                case DataHolder.Type.Bool:
                    dataHolder.SetValue(EditorUtils.Checkbox(this, "Default Value", dataHolder.BoolValue));
                    break;
                case DataHolder.Type.Number:
                    dataHolder.SetValue(EditorUtils.FloatField(this, "Default Value", dataHolder.FloatValue));
                    break;
                case DataHolder.Type.Integer:
                    dataHolder.SetValue(EditorUtils.IntField(this, "Default Value", dataHolder.IntValue));
                    break;
                case DataHolder.Type.Text:
                    dataHolder.SetValue(EditorUtils.TextField(this, "Default Value", dataHolder.Value));
                    break;
            }
        }

        StorePrefabConflict(dataHolder);
    }

}
