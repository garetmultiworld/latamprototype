﻿using UnityEngine;

public class SetDataTrigger : TriggerInterface
{
    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;
    public string Value;
    public float NumberValue;
    public int IntegerValue;
    public bool BoolValue;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default://assumes string
                        data.SetValue(Value);
                        break;
                    case DataHolder.Type.Number:
                        data.SetValue(NumberValue);
                        break;
                    case DataHolder.Type.Integer:
                        data.SetValue(IntegerValue);
                        break;
                    case DataHolder.Type.Bool:
                        data.SetValue(BoolValue);
                        break;
                }
                break;
            }
        }
    }
}
