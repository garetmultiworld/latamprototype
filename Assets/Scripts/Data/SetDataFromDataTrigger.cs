﻿using UnityEngine;

public class SetDataFromDataTrigger : TriggerInterface
{

#if UNITY_EDITOR
    [HideInInspector]
    public bool showSetDataFromDataTrigger=true;
#endif

    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;
    public GameObject HolderDestination;
    public bool FromTargetDestination;
    public string LabelDataDestination;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder Destination=null;
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (HolderDestination != null)
        {
            tempHolder = HolderDestination;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTargetDestination)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelDataDestination.Equals(data.Name))
            {
                Destination = data;
                break;
            }
        }
        if (Destination == null)
        {
            return;
        }

        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default://assumes string
                        Destination.SetValue(data.Value);
                        break;
                    case DataHolder.Type.Number:
                        Destination.SetValue(data.FloatValue);
                        break;
                    case DataHolder.Type.Integer:
                        Destination.SetValue(data.IntValue);
                        break;
                }
                break;
            }
        }
    }

}
