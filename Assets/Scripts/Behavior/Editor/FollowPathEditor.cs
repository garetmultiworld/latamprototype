﻿using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(FollowPathState))]
public class FollowPathStateEditor : EditorBase
{
    protected FollowPathState followPath;

    protected void SetUpPrefabConflict(FollowPathState path)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(path, "FollowPathState");
    }

    protected void StorePrefabConflict(FollowPathState path)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(path);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(path.gameObject.scene);
    }

    public void OnSceneGUI()
    {
        followPath = (FollowPathState)target;
        PathLocation location;
        Rigidbody2D body = followPath.GetComponent<Rigidbody2D>();
        if (body == null)
        {
            EditorGUILayout.HelpBox("This object must have a Rigidbody2D", MessageType.Warning);
            if (GUILayout.Button("Add Rigidbody2D"))
            {
                _ = followPath.gameObject.AddComponent(typeof(Rigidbody2D)) as Rigidbody2D;
                _hadChanges = true;
            }
        }
        for (int iLocation=0;iLocation<followPath.path.Locations.Length;iLocation++) {
            location = followPath.path.Locations[iLocation];
            Vector3 prevPos = location.Location;
            Handles.Label(location.Location, iLocation.ToString());
            if (followPath.path.GlobalPositioning)
            {
                location.Location = Handles.PositionHandle(location.Location, Quaternion.identity);
            }
            else
            {
                location.Location = Handles.PositionHandle(location.Location+ followPath.transform.position, Quaternion.identity)- followPath.transform.position;
            }
            if (prevPos != location.Location)
            {
                _hadChanges = true;
            }
        }
    }

    public override void OnInspectorGUI()
    {
        followPath = (FollowPathState)target;
        SetUpPrefabConflict(followPath);
        FollowPathStateInspector();
        followPath.showPath = EditorGUILayout.Foldout(
            followPath.showPath,
            "Path"
        );
        if (followPath.showPath)
        {
            PathInspector(followPath.path);
        }
        StorePrefabConflict(followPath);
    }

    protected void FollowPathStateInspector()
    {
        followPath.OneByOne = EditorUtils.Checkbox(
            this,
            "One By One",
            followPath.OneByOne
        );
        followPath.MovementSpeed = EditorUtils.FloatField(this,"Movement Speed", followPath.MovementSpeed);
    }

    protected void PathInspector(Path path)
    {
        path.CycleOption = (Path.CycleOptions)EditorUtils.EnumPopup(this,"Cycle Type",path.CycleOption);
        path.movementDirection = (Path.MovementDirection)EditorUtils.EnumPopup(this,"Initial Movement",path.movementDirection);
        path.MinDistanceToGoal = EditorUtils.FloatField(this,"Min Distance To Goal", path.MinDistanceToGoal);
        path.GlobalPositioning = EditorUtils.Checkbox(
            this,
            "Global Positions",
            path.GlobalPositioning
        );
        path.showLocations = EditorGUILayout.Foldout(
            path.showLocations,
            "Locations"
        );
        if (path.showLocations)
        {
            if (path.Locations.Length == 0)
            {
                if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
                {
                    Array.Resize(ref path.Locations, path.Locations.Length + 1);
                    path.Locations[path.Locations.Length - 1] = new PathLocation();
                    _hadChanges = true;
                }
            }
            for (int iLocation = 0; iLocation < path.Locations.Length; iLocation++)
            {
                DrawLocationItem(path, iLocation);
            }
        }
    }

    protected void DrawLocationItem(Path path,int index)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            EditorArrayUtils.RemoveAt(ref path.Locations, index);
            deleted = true;
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertBefore(ref path.Locations, index);
            path.Locations[index] = new PathLocation();
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertAfter(ref path.Locations, index);
            path.Locations[index + 1] = new PathLocation();
            _hadChanges = true;
        }
        if (index == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveUp(ref path.Locations, index);
                _hadChanges = true;
            }
        }
        if (index == (path.Locations.Length - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveDown(ref path.Locations, index);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawLocationItemDetails(path, index);
        }
    }

    protected void DrawLocationItemDetails(Path path, int index)
    {
        path.Locations[index].showLocation = EditorGUILayout.Foldout(
            path.Locations[index].showLocation,
            index.ToString()
        );
        if (path.Locations[index].showLocation)
        {
            path.Locations[index].WaitTimeAfterArrival = EditorUtils.FloatField(this,"Wait Time After Arrival", path.Locations[index].WaitTimeAfterArrival);
            path.Locations[index].OnArriveTrigger = EditorUtils.SelectTrigger(this,"On Arrive", path.Locations[index].OnArriveTrigger);
            path.Locations[index].OnLeaveTrigger = EditorUtils.SelectTrigger(this,"On Leave", path.Locations[index].OnLeaveTrigger);


            Vector3 prevPos= path.Locations[index].Location;
            path.Locations[index].Location = EditorGUILayout.Vector3Field("Location:", path.Locations[index].Location);
            if (prevPos != path.Locations[index].Location)
            {
                _hadChanges = true;
            }

        }
    }
}
