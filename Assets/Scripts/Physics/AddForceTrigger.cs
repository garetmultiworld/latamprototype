﻿using UnityEngine;

public class AddForceTrigger : TriggerInterface
{
    public Rigidbody body;
    public Vector3 direction;
    public float force;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        body.AddForce(direction * force);
    }
}
