﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelSectionConnector), true)]
public class LevelSectionConnectorEditor : Editor
{

    protected bool _hadChanges = false;

    protected void SetUpPrefabConflict(LevelSectionConnector levelSectionConnector)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(levelSectionConnector, "LevelSectionConnector");
    }

    protected void StorePrefabConflict(LevelSectionConnector levelSectionConnector)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(levelSectionConnector);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(levelSectionConnector.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        _hadChanges = false;
        LevelSectionConnector levelSectionConnector = (LevelSectionConnector)target;
        SetUpPrefabConflict(levelSectionConnector);
        string prevString = levelSectionConnector.name;
        levelSectionConnector.name = EditorGUILayout.TextField("Name: ", levelSectionConnector.name);
        if (
            (string.IsNullOrEmpty(prevString)&& !string.IsNullOrEmpty(prevString) )||
            (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(prevString)) ||
            (prevString!=null&&!prevString.Equals(levelSectionConnector.name))
        )
        {
            _hadChanges = true;
        }
        if (EditorApplication.isPlaying)
        {
            _ = (LevelSectionConnector)EditorGUILayout.ObjectField(
                "ConnectedTo",
                levelSectionConnector.ConnectedTo,
                typeof(LevelSectionConnector),
                true
            );
        }
        LevelSection prevLevelSection = levelSectionConnector.levelSection;
        levelSectionConnector.levelSection=(LevelSection)EditorGUILayout.ObjectField(
            "Level Section",
            levelSectionConnector.levelSection,
            typeof(LevelSection),
            true
        );
        if(prevLevelSection!= levelSectionConnector.levelSection)
        {
            _hadChanges = true;
        }
        GUILayout.BeginHorizontal();
        if (levelSectionConnector.Items.Count== 0)
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
            {
                levelSectionConnector.Items.Add(new LevelSectionConnectorItem());
                _hadChanges = true;
            }
        }
        if (GUILayout.Button("Equal Chances"))
        {
            float newChance = 100f / (float)levelSectionConnector.Items.Count;
            for (int iItem = 0; iItem < levelSectionConnector.Items.Count; iItem++)
            {
                levelSectionConnector.Items[iItem].Chances = newChance;
            }
            _hadChanges = true;
        }
        GUILayout.EndHorizontal();
        for (int iItem = 0; iItem < levelSectionConnector.Items.Count; iItem++)
        {
            DrawItem(levelSectionConnector, iItem);
        }
        StorePrefabConflict(levelSectionConnector);
    }

    protected void DrawItem(LevelSectionConnector levelSectionConnector, int iItem)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            levelSectionConnector.Items.RemoveAt(iItem);
            _hadChanges = true;
            deleted = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            levelSectionConnector.Items.Insert(iItem, new LevelSectionConnectorItem());
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            levelSectionConnector.Items.Insert(iItem+1, new LevelSectionConnectorItem());
            _hadChanges = true;
        }
        if (iItem == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                LevelSectionConnectorItem temp= levelSectionConnector.Items[iItem];
                levelSectionConnector.Items.RemoveAt(iItem);
                levelSectionConnector.Items.Insert(iItem - 1, temp);
                _hadChanges = true;
            }
        }
        if (iItem == (levelSectionConnector.Items.Count - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                LevelSectionConnectorItem temp = levelSectionConnector.Items[iItem];
                levelSectionConnector.Items.RemoveAt(iItem);
                levelSectionConnector.Items.Insert(iItem + 1, temp);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawItemDetails(levelSectionConnector, iItem);
        }
    }

    protected void DrawItemDetails(LevelSectionConnector levelSectionConnector, int iItem)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(iItem.ToString());
        string prevString = levelSectionConnector.Items[iItem].ConnectorName;
        levelSectionConnector.Items[iItem].ConnectorName = EditorGUILayout.TextField(
            "Connector Name",
            levelSectionConnector.Items[iItem].ConnectorName
        );
        if (
            (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(levelSectionConnector.Items[iItem].ConnectorName)) ||
            (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(levelSectionConnector.Items[iItem].ConnectorName)) ||
            (prevString != null && !prevString.Equals(levelSectionConnector.Items[iItem].ConnectorName))
        )
        {
            _hadChanges = true;
        }
        GUILayout.EndHorizontal();
        float prevFloat = levelSectionConnector.Items[iItem].Chances;
        levelSectionConnector.Items[iItem].Chances = EditorGUILayout.Slider(levelSectionConnector.Items[iItem].Chances, 0, 100);
        if(prevFloat!= levelSectionConnector.Items[iItem].Chances)
        {
            _hadChanges = true;
        }
    }
}
