﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelSection), true)]
public class LevelSectionEditor : EditorBase
{
    protected bool _hadChanges = false;

    protected void SetUpPrefabConflict(LevelSection levelSection)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(levelSection, "LevelSectionConnector");
    }

    protected void StorePrefabConflict(LevelSection levelSection)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(levelSection);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(levelSection.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        _hadChanges = false;
        LevelSection levelSection = (LevelSection)target;
        SetUpPrefabConflict(levelSection);
        PlayerStart prevStart = levelSection.playerStart;
        levelSection.playerStart = (PlayerStart)EditorGUILayout.ObjectField(
            "Player Start",
            levelSection.playerStart,
            typeof(PlayerStart),
            true
        );
        if(prevStart!= levelSection.playerStart)
        {
            _hadChanges = true;
        }
        levelSection.OnArriveTrigger = EditorUtils.SelectTrigger(this,"On Player Arrive", levelSection.OnArriveTrigger);
        levelSection.OnLeaveTrigger = EditorUtils.SelectTrigger(this,"On Player Leave", levelSection.OnLeaveTrigger);
        GUILayout.Label("Connectors");
        GUILayout.BeginHorizontal();
        if (levelSection.levelSectionConnectors.Count == 0)
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
            {
                levelSection.levelSectionConnectors.Add(new LevelSectionConnector());
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        for (int iItem = 0; iItem < levelSection.levelSectionConnectors.Count; iItem++)
        {
            DrawItem(levelSection, iItem);
        }
        StorePrefabConflict(levelSection);
    }

    protected void DrawItem(LevelSection levelSection, int iItem)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            levelSection.levelSectionConnectors.RemoveAt(iItem);
            _hadChanges = true;
            deleted = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            levelSection.levelSectionConnectors.Insert(iItem, new LevelSectionConnector());
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            levelSection.levelSectionConnectors.Insert(iItem + 1, new LevelSectionConnector());
            _hadChanges = true;
        }
        if (iItem == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                LevelSectionConnector temp = levelSection.levelSectionConnectors[iItem];
                levelSection.levelSectionConnectors.RemoveAt(iItem);
                levelSection.levelSectionConnectors.Insert(iItem - 1, temp);
                _hadChanges = true;
            }
        }
        if (iItem == (levelSection.levelSectionConnectors.Count - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                LevelSectionConnector temp = levelSection.levelSectionConnectors[iItem];
                levelSection.levelSectionConnectors.RemoveAt(iItem);
                levelSection.levelSectionConnectors.Insert(iItem + 1, temp);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawItemDetails(levelSection, iItem);
        }
    }

    protected void DrawItemDetails(LevelSection levelSection, int iItem)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(iItem.ToString());

        LevelSectionConnector prevConnector = levelSection.levelSectionConnectors[iItem];
        levelSection.levelSectionConnectors[iItem] = (LevelSectionConnector)EditorGUILayout.ObjectField(
            levelSection.levelSectionConnectors[iItem],
            typeof(LevelSectionConnector),
            true
        );
        if (prevConnector != levelSection.levelSectionConnectors[iItem])
        {
            _hadChanges = true;
        }
        GUILayout.EndHorizontal();
    }
}
