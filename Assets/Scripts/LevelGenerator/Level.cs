﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Level",menuName ="Multiworld/Level")]
public class Level : ScriptableObject
{

    public new string name;
    public List<GameObject> StartingSections=new List<GameObject>();
    public List<GameObject> levelSections = new List<GameObject>();
    public int MinSectionAmount;
    public int MaxSectionAmount;

}
