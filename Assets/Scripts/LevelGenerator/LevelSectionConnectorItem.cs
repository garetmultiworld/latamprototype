﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelSectionConnectorItem
{

    public string ConnectorName;
    [Range(0f, 100f)]
    public float Chances;

}
