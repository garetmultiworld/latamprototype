﻿using System.Collections.Generic;
using UnityEngine;

public class LevelSectionConnector : MonoBehaviour
{

    public new string name;
    public List<LevelSectionConnectorItem> Items=new List<LevelSectionConnectorItem>();

    public LevelSection levelSection;
    
    public LevelSectionConnector ConnectedTo;
    
    public string lastConnected;

    public string GetNextConnectorName()
    {
        float chance=Random.Range(0f,100f);
        float cummulative = 0;
        foreach (LevelSectionConnectorItem item in Items)
        {
            if (chance < (item.Chances+cummulative))
            {
                lastConnected = item.ConnectorName;
                return item.ConnectorName;
            }
            cummulative += item.Chances;
        }
        return "";
    }

}
