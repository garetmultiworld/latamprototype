﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelTrigger : TriggerInterface
{

    protected DungeonGenerator dungeonGenerator;

    void Start()
    {
        dungeonGenerator = FindObjectOfType<DungeonGenerator>();
    }

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (dungeonGenerator != null)
        {
            dungeonGenerator.NextLevel();
        }
    }
}
