﻿using UnityEngine;

public class EnableMultipleObjectTrigger : TriggerInterface
{
    public GameObject[] Objects=new GameObject[0];

    public override void Cancel()
    {
        foreach (GameObject gameObject in Objects)
        {
            gameObject.SetActive(false);
        }
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach(GameObject gameObject in Objects)
        {
            gameObject.SetActive(true);
        }
    }
}
