﻿using System.Collections;
using UnityEngine;

public class ChangePositionTrigger: TriggerInterface
{

    public Transform NewPosition;
    public GameObject Object;
    public bool Animate;
    public float MinTime = 1;
    public float MaxTime = 2;
    public AnimationCurve AnimCurveX;
    public AnimationCurve AnimCurveY;
    public TriggerInterface TriggerOnArrive;
    public bool Local;
    public bool DataFromTarget;

    private bool IsAnimating = false;
    private Vector3 StartingPos;
    private Vector3 TargetPos;
    private float TotalTime;
    private float CurrentTimer = 0;
    private GameObject TheObject;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (DataFromTarget)
        {
            TheObject = Object.GetComponent<TargetHolder>().Target;
        }
        else
        {
            TheObject = Object;
        }
        TargetPos = (Local) ? NewPosition.localPosition : NewPosition.position;
        if (Animate)
        {
            StartingPos = (Local) ? TheObject.transform.localPosition : TheObject.transform.position;
            TotalTime = UnityEngine.Random.Range(MinTime, MaxTime);
            CurrentTimer = 0;
            IsAnimating = true;
            StartCoroutine(ChangePosition());
        }
        else
        {
            if (Local)
            {
                TheObject.transform.localPosition = TargetPos;
            }
            else
            {
                TheObject.transform.position = TargetPos;
            }
            if (TriggerOnArrive != null)
            {
                TriggerOnArrive.Fire(this);
            }
        }
    }

    private IEnumerator ChangePosition()
    {
        while (IsAnimating)
        {
            float t = CurrentTimer / TotalTime;
            Vector3 newPos = new Vector3(
                StartingPos.x + ((TargetPos.x - StartingPos.x) * AnimCurveX.Evaluate(t)),
                StartingPos.y + ((TargetPos.y - StartingPos.y) * AnimCurveY.Evaluate(t)),
                TheObject.transform.position.z
            );
            CurrentTimer += Time.deltaTime;
            if (CurrentTimer >= TotalTime)
            {
                newPos.x = TargetPos.x;
                newPos.y = TargetPos.y;
                IsAnimating = false;
                if (TriggerOnArrive != null)
                {
                    TriggerOnArrive.Fire(this);
                }
            }
            if (Local)
            {
                TheObject.transform.localPosition = newPos;
            }
            else
            {
                TheObject.transform.position = newPos;
            }
            yield return null;
        }
    }

}
