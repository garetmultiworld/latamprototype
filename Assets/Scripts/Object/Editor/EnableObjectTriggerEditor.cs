﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EnableObjectTrigger))] 
public class EnableObjectTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(EnableObjectTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "EnableObjectTrigger");
    }
     
    protected void StorePrefabConflict(EnableObjectTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EnableObjectTrigger enableObjectTrigger = (EnableObjectTrigger)target;
        SetUpPrefabConflict(enableObjectTrigger);
        enableObjectTrigger.TheObject = EditorUtils.GameObjectField(
            this,
            "Object",
            enableObjectTrigger.TheObject
        );
        StorePrefabConflict(enableObjectTrigger);
    }

}
