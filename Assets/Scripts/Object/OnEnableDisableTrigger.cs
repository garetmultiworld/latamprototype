﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableDisableTrigger : MonoBehaviour
{

    public TriggerInterface OnEnableTrigger;
    public TriggerInterface OnDisableTrigger;

    void OnEnable()
    {
        if (OnEnableTrigger != null)
        {
            OnEnableTrigger.Fire();
        }
    }

    void OnDisable()
    {
        if (OnDisableTrigger != null)
        {
            OnDisableTrigger.IgnoreDisabled = true;
            OnDisableTrigger.Fire();
            OnDisableTrigger.IgnoreDisabled = false;
        }
    }

}
