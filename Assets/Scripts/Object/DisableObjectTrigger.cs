﻿using UnityEngine;

public class DisableObjectTrigger : TriggerInterface
{

    public GameObject TheObject;

    public override void Cancel()
    {
        TheObject.SetActive(true);
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        TheObject.SetActive(false);
    }
}
