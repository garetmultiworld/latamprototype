﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogInteractionItem
{
    public float TimeBefore;
    public float TimeAfter;
    public string Type;
    public string I18nFolder;
    public string I18nItem;
    public TriggerInterface TriggerOnShow;
    public TriggerInterface TriggerOnHide;
    [Header("Location")]
    public GameObject Parent;
    public RectBounds Bounds;
}
