﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmDialogManager : MonoBehaviour
{

    #region Static Instance
    private static ConfirmDialogManager instance;
    public static ConfirmDialogManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<ConfirmDialogManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned ConfirmDialogManager", typeof(ConfirmDialogManager)).GetComponent<ConfirmDialogManager>();
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    [HideInInspector]
    protected Dictionary<string, ConfirmDialog> dialogs = new Dictionary<string, ConfirmDialog>();

    ConfirmDialogManager()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void RegisterDialog(ConfirmDialog dialog)
    {
        dialogs.Add(dialog.Name, dialog);
    }

    public void ShowConfirm(string dialogName,TriggerInterface OnYes,TriggerInterface OnNo)
    {
        if (!dialogs.ContainsKey(dialogName))
        {
            return;
        }
        ConfirmDialog dialog = dialogs[dialogName];
        if (dialog == null)
        {
            return;
        }
        if (dialog.OnShow != null)
        {
            dialog.OnShow.Fire();
        }
        dialog.OnYesCallback = OnYes;
        dialog.OnNoCallback = OnNo;
        dialog.gameObject.SetActive(true);
    }

}
